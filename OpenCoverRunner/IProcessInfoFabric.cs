﻿using System.Diagnostics;

namespace OpenCoverRunner
{
    public interface IProcessInfoFabric
    {
        ProcessStartInfo CreateOpenCoverProcessInfo(string projectFile);
        ProcessStartInfo CreateReportGeneratorProcessInfo();
    }
}