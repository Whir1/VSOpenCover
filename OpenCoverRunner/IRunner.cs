using System;
using System.Diagnostics;

namespace OpenCoverRunner
{
    public interface IRunner
    {
        void Run(ProcessStartInfo processInfo, Action<string> writeOutput);
    }
}