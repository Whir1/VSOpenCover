﻿using System.Collections.Generic;
using VSOpenCover.SharedTypes;

namespace OpenCoverRunner.ProcessInfoBuilders.ArgumentFormatters
{
    public class EmptyArgumentFormatter:IArgumentFormatter
    {
        public string BuildArguments(Dictionary<string, string> arguments)
        {
            return "";
        }

        public ProcessType ProcessType { get; private set; }
    }
}