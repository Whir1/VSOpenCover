﻿using System.Collections.Generic;
using VSOpenCover.SharedTypes;

namespace OpenCoverRunner.ProcessInfoBuilders.ArgumentFormatters
{
    public interface IArgumentFormatter
    {
        string BuildArguments(Dictionary<string, string> arguments);
        
        ProcessType ProcessType { get; }
    }
}