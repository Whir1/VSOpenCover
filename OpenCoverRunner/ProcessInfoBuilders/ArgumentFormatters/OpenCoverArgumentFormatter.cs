﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using OpenCoverRunner.Annotations;
using VSOpenCover.SharedTypes;
using VSOpenCover.SharedTypes.Settings;

namespace OpenCoverRunner.ProcessInfoBuilders.ArgumentFormatters
{
    [UsedImplicitly]
    public class OpenCoverArgumentFormatter : IArgumentFormatter
    {
        private readonly IOpenCoverSettings _settings;
        

        public OpenCoverArgumentFormatter(IOpenCoverSettings settings)
        {
            _settings = settings;
        }

        public string BuildArguments(Dictionary<string, string> arguments)
        {
            //"-register:user -mergebyhash -target:"%testRunnerPath%" -targetargs:"%targetFilePath% /noshadow /framework:net-4.0" -filter:"+[CoBrand*]* -[*SharedTypes]* -[*Tests]*" -output:%outputFilePath% "
            var sb = new StringBuilder();
            sb.Append("-register:user ");
            sb.Append("-mergebyhash ");
            sb.AppendFormat("-target:\"{0}\" ", _settings.TestRunnerExePath);
            sb.AppendFormat("-targetargs:\"{0} ", arguments[ArgumentKeys.Project]);
            sb.Append("/noshadow ");
            sb.AppendFormat("/framework:{0}\" ", _settings.Framework);
            sb.AppendFormat("-filter:\"{0}\" ", _settings.OpenCoverFilter);

            var fileInfo = new FileInfo(arguments[ArgumentKeys.Project]);
            sb.AppendFormat("-output:{0}.xml", Path.Combine(_settings.OutputDirectory, fileInfo.Name));
            return sb.ToString();
        }

        public ProcessType ProcessType
        {
            get { return ProcessType.OpenCover; }
        }
    }
}