﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using OpenCoverRunner.Annotations;
using VSOpenCover.SharedTypes;
using VSOpenCover.SharedTypes.Settings;

namespace OpenCoverRunner.ProcessInfoBuilders.ArgumentFormatters
{
    [UsedImplicitly]
    public class ReportGeneratorArgumentFormatter : IArgumentFormatter
    {
        private readonly IReportGeneratorSettings _settings;

        public ReportGeneratorArgumentFormatter(IReportGeneratorSettings settings)
        {
            _settings = settings;
        }

        public string BuildArguments(Dictionary<string, string> arguments)
        {
            //%reportGeneratorDir%\ReportGenerator.exe -reports:%outputFilePath% -targetdir:%reportDir% -reporttypes:html
            var sb = new StringBuilder();
            sb.AppendFormat("-reports:{0} ", _settings.InputFilesMask);
            sb.AppendFormat("-targetdir:{0} ", _settings.ReportGeneratorOutputDirectory);
            sb.AppendFormat("-reporttypes:{0}", _settings.OutputType.ToString().ToUpper());
            return sb.ToString();
        }

        public ProcessType ProcessType
        {
            get { return ProcessType.ReportGenerator; }
        }
    }
}