﻿using System.Collections.Generic;
using System.Diagnostics;
using VSOpenCover.SharedTypes;

namespace OpenCoverRunner.ProcessInfoBuilders
{
    public interface IProcessInfoBuilder
    {
        ProcessStartInfo Build(Dictionary<string, string> arguments);

        ProcessType ProcessType { get; }
    }
}