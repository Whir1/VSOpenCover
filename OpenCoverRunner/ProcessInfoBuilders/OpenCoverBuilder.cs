﻿using System.Collections.Generic;
using System.Diagnostics;
using OpenCoverRunner.Annotations;
using OpenCoverRunner.ProcessInfoBuilders.ArgumentFormatters;
using VSOpenCover.SharedTypes;
using VSOpenCover.SharedTypes.Settings;

namespace OpenCoverRunner.ProcessInfoBuilders
{
    [UsedImplicitly]
    public class OpenCoverBuilder: ProcessInfoBuilderBase
    {
        private readonly IOpenCoverSettings _settings;

        public OpenCoverBuilder(IArgumentFormatter[] argumentFormatters, IOpenCoverSettings settings):base(argumentFormatters)
        {
            _settings = settings;
        }

        public override ProcessStartInfo Build(Dictionary<string, string> arguments)
        {
            var processInfo = base.Build(arguments);
            processInfo.FileName = _settings.OpenCoverExePath;
            return processInfo;
        }

        public override ProcessType ProcessType
        {
            get { return ProcessType.OpenCover; }
        }
    }
}