﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using OpenCoverRunner.ProcessInfoBuilders.ArgumentFormatters;
using VSOpenCover.SharedTypes;

namespace OpenCoverRunner.ProcessInfoBuilders
{
    public abstract class ProcessInfoBuilderBase : IProcessInfoBuilder
    {
        private readonly IArgumentFormatter[] _argumentFormatters;

        protected ProcessInfoBuilderBase(IArgumentFormatter[] argumentFormatters)
        {
            _argumentFormatters = argumentFormatters;
        }

        public virtual ProcessStartInfo Build(Dictionary<string, string> arguments)
        {
            var processInfo = new ProcessStartInfo
                              {
                                    RedirectStandardOutput = true,
                                    UseShellExecute = false,
                                    CreateNoWindow = true,
                                    RedirectStandardError = true,
                                    Arguments = FormatArguments(arguments),
                              };
            return processInfo;
        }

        private string FormatArguments(Dictionary<string, string> arguments)
        {
            if (arguments == null)
            {
                return "";
            }

            IArgumentFormatter argumentFormatter =
                    _argumentFormatters.FirstOrDefault(c => c.ProcessType == ProcessType) ??
                    new EmptyArgumentFormatter();
            return argumentFormatter.BuildArguments(arguments);
        }

        public abstract ProcessType ProcessType { get; }
    }
}
