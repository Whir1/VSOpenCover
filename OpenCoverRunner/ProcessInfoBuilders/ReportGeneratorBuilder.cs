﻿using System.Collections.Generic;
using System.Diagnostics;
using OpenCoverRunner.Annotations;
using OpenCoverRunner.ProcessInfoBuilders.ArgumentFormatters;
using VSOpenCover.SharedTypes;
using VSOpenCover.SharedTypes.Settings;

namespace OpenCoverRunner.ProcessInfoBuilders
{
    [UsedImplicitly]
    public class ReportGeneratorBuilder : ProcessInfoBuilderBase
    {
        private readonly IReportGeneratorSettings _settings;

        public ReportGeneratorBuilder(IArgumentFormatter[] argumentFormatter, IReportGeneratorSettings settings):base(argumentFormatter)
        {
            _settings = settings;
        }

        public override ProcessStartInfo Build(Dictionary<string, string> arguments)
        {
            var processInfo = base.Build(arguments);
            processInfo.FileName = _settings.ReportGeneratorExePath;
            return processInfo;
        }

        public override ProcessType ProcessType
        {
            get { return ProcessType.ReportGenerator; }
        }
    }
}