﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using OpenCoverRunner.Annotations;
using OpenCoverRunner.ProcessInfoBuilders;
using OpenCoverRunner.ProcessInfoBuilders.ArgumentFormatters;
using VSOpenCover.SharedTypes;

namespace OpenCoverRunner
{
    [UsedImplicitly]
    public sealed class ProcessInfoFabric : IProcessInfoFabric
    {
        private readonly IProcessInfoBuilder[] _builders;

        public ProcessInfoFabric(IProcessInfoBuilder[] builders)
        {
            _builders = builders;
        }

        public ProcessStartInfo CreateOpenCoverProcessInfo(string projectFile)
        {
            IProcessInfoBuilder builder = _builders.First(c => c.ProcessType == ProcessType.OpenCover);
            
            var arguments = new Dictionary<string, string> { { ArgumentKeys.Project, projectFile } };
            ProcessStartInfo resultProcessInfo = builder.Build(arguments);

            return resultProcessInfo;
        }

        public ProcessStartInfo CreateReportGeneratorProcessInfo()
        {
            IProcessInfoBuilder builder = _builders.First(c => c.ProcessType == ProcessType.ReportGenerator);
            return builder.Build(new Dictionary<string, string>());
        }
    }
}