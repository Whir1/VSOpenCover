﻿using System;
using System.Diagnostics;
using OpenCoverRunner.Annotations;

namespace OpenCoverRunner
{
    [UsedImplicitly]
    public sealed class Runner : IRunner
    {
        public void Run(ProcessStartInfo processInfo, Action<string> writeOutput)
        {
            var process = new Process {StartInfo = processInfo};
            process.OutputDataReceived += (sender, args) => writeOutput(args.Data);
            process.Start();
            process.BeginOutputReadLine();
            process.WaitForExit();
        }

    }
}