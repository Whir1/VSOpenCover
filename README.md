VSOpenCover
===========

Unit test coverage addon for visual studio

Before use you should download:
Nunit from http://www.nunit.org/index.php?p=download
Open Cover from http://opencover.codeplex.com/
Report Generator from http://reportgenerator.codeplex.com/

In Visual Studio settings set path to exe files.
