﻿using System;
using System.Collections.Generic;
using VSOpenCover.CoverageProcessing.Annotations;
using VSOpenCover.CoverageProcessing.Processors;

namespace VSOpenCover.CoverageProcessing
{
    [UsedImplicitly]
    public class CoverageProcessor : ICoverageProcessor
    {
        private readonly IReportProcessor _reportProcessor;
        private readonly IFileProcessor _fileProcessor;

        public CoverageProcessor(IReportProcessor reportProcessor, IFileProcessor fileProcessor)
        {
            _reportProcessor = reportProcessor;
            _fileProcessor = fileProcessor;
        }

        public string MakeCoverageReport(IEnumerable<string> testProjects, Action<string> outAction)
        {
            _fileProcessor.ClearOldResults();
            CoverAssembliesWithOpenCover(testProjects, outAction);
            MakeReport(outAction);
            return _fileProcessor.GetResultFilePath();
        }

        private void CoverAssembliesWithOpenCover(IEnumerable<string> testProjects, Action<string> outAction)
        {
            foreach (var project in testProjects)
            {
                _reportProcessor.MakeOpenCoverReport(project, outAction);
            }
        }

        private void MakeReport(Action<string> outAction)
        {
            _reportProcessor.MakeReportGeneratorReport(outAction);
        }
    }
}