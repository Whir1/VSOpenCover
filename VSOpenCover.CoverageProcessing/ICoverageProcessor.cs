﻿using System;
using System.Collections.Generic;

namespace VSOpenCover.CoverageProcessing
{
    public interface ICoverageProcessor
    {
        string MakeCoverageReport(IEnumerable<string> testProjects, Action<string> outAction);
    }
}