﻿using System;
using System.IO;
using VSOpenCover.CoverageProcessing.Annotations;
using VSOpenCover.SharedTypes;
using VSOpenCover.SharedTypes.Settings;

namespace VSOpenCover.CoverageProcessing.Processors
{
    [UsedImplicitly]
    public class FileProcessor : IFileProcessor
    {
        private readonly IVsOpenCoverSettings _settings;

        public FileProcessor(IVsOpenCoverSettings settings)
        {
            _settings = settings;
        }

        public void ClearOldResults()
        {
            var dir = _settings.OutputDirectory;
            if (Directory.Exists(dir))
                Directory.Delete(dir, true);
            Directory.CreateDirectory(dir);
        }

        public string GetResultFilePath()
        {
            if (_settings.OutputType == ReportGeneratorOutputType.Html)
            {
                var path = Path.Combine(_settings.ReportGeneratorOutputDirectory, "index.htm");
                var fullPath = Path.GetFullPath(path);
                return fullPath;
            }
            if (_settings.OutputType == ReportGeneratorOutputType.Xml)
            {
                var path = Path.Combine(_settings.ReportGeneratorOutputDirectory, "Summary.xml");
                var fullPath = Path.GetFullPath(path);
                return fullPath;
            }
            throw new ArgumentException("Wrong output type");
        }
    }
}