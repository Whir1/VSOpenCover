﻿namespace VSOpenCover.CoverageProcessing.Processors
{
    public interface IFileProcessor
    {
        void ClearOldResults();
        string GetResultFilePath();
    }
}