using System;

namespace VSOpenCover.CoverageProcessing.Processors
{
    public interface IReportProcessor
    {
        void MakeReportGeneratorReport(Action<string> outAction);
        void MakeOpenCoverReport(string testDll, Action<string> outAction);
    }
}