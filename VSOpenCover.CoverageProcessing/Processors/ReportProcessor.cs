using System;
using System.Diagnostics;
using OpenCoverRunner;
using VSOpenCover.CoverageProcessing.Annotations;

namespace VSOpenCover.CoverageProcessing.Processors
{
    [UsedImplicitly]
    public sealed class ReportProcessor : IReportProcessor
    {
        private readonly IProcessInfoFabric _processInfoFabric;
        private readonly IRunner _runner;

        public ReportProcessor(IProcessInfoFabric processInfoFabric, IRunner runner)
        {
            _processInfoFabric = processInfoFabric;
            _runner = runner;
        }

        public void MakeReportGeneratorReport(Action<string> outAction)
        {
            ProcessStartInfo processInfo = _processInfoFabric.CreateReportGeneratorProcessInfo();
            _runner.Run(processInfo, outAction);
        }

        public void MakeOpenCoverReport(string testDll, Action<string> outAction)
        {
            ProcessStartInfo processInfo = _processInfoFabric.CreateOpenCoverProcessInfo(testDll);
            _runner.Run(processInfo, outAction);
        }
    }
}