﻿using System;
using System.ComponentModel.Design;
using System.Windows.Input;
using System.Windows.Threading;
using Microsoft.VisualStudio.Shell;

namespace VSOpenCover.VSPackage.Commands
{
    public abstract class CommandBase : OleMenuCommand, ICommand
    {
        private readonly VSOpenCoverPackage _package;

        public CommandBase(VSOpenCoverPackage package, CommandID commandId)
            : base(CommandExecuted, commandId)
        {
            _package = package;
        }

        protected VSOpenCoverPackage Package
        {
            get { return _package; }
        }

        private static void CommandExecuted(object sender, EventArgs e)
        {
            var command = (CommandBase)sender;

            var eventArgs = (OleMenuCmdEventArgs)e;

            command.OnExecute(eventArgs);
        }

        protected abstract void OnExecute(OleMenuCmdEventArgs eventArgs);

        public bool CanExecute(object parameter)
        {
            return Dispatcher.CurrentDispatcher.CheckAccess();
        }

        public void Execute(object parameter)
        {
            Invoke(parameter);
        }

        public event EventHandler CanExecuteChanged;
    }
}