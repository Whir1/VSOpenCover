﻿using System;
using System.ComponentModel.Design;
using Microsoft.VisualStudio.Shell;
using VsOpenCover.ReportGeneratorResultParser;
using VsOpenCover.ReportGeneratorResultParser.Model.Summary;
using VSOpenCover.SharedTypes;
using VSOpenCover.SharedTypes.Settings;

namespace VSOpenCover.VSPackage.Commands
{
    public class OpenCoverageResultsCommand : CommandBase
    {
        public OpenCoverageResultsCommand(VSOpenCoverPackage package, CommandID commandId) : base(package, commandId)
        {
        }

        protected override void OnExecute(OleMenuCmdEventArgs eventArgs)
        {
            IVsOpenCoverSettings settings = Package.UnityContainer.ResolveSettings();
            if (settings.OutputType == ReportGeneratorOutputType.Html)
            {
                System.Diagnostics.Process.Start((string)eventArgs.InValue);
            }
            if (settings.OutputType == ReportGeneratorOutputType.Xml)
            {
                IReportParser parser = Package.UnityContainer.ResolverReportParser();
                    
                SummaryCoverageReport model = parser.ParseSummaryReportFile((string)eventArgs.InValue);
                Package.Commands[typeof(ShowToolWindowCommand)].Invoke(model, default(IntPtr));
            }
        }
    }
}