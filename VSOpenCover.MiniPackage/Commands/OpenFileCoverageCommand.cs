﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Input;
using Microsoft.VisualStudio.Shell;
using VsOpenCover.ReportGeneratorResultParser.Model.Summary;

namespace VSOpenCover.VSPackage.Commands
{
    public class OpenFileCoverageCommand : CommandBase, ICommand
    {
        public OpenFileCoverageCommand(VSOpenCoverPackage package, CommandID commandId) : base(package, commandId)
        {
        }

        protected override void OnExecute(OleMenuCmdEventArgs eventArgs)
        {
            var parameters = (object[]) eventArgs.InValue;
            var classEntity = (Class)(parameters[0]);
            var assemblies = (IEnumerable<Assembly>)(parameters[1]);
            var assembly = (from a in assemblies
                            where a.Classes.Contains(classEntity)
                            select a).First();

            var parser = Package.UnityContainer.ResolverReportParser();
            var settings = Package.UnityContainer.ResolveSettings();

            var classModel = parser.ParseClassCoverageReportFromModel(assembly, classEntity.Name, settings.ReportGeneratorOutputDirectory);
            OpenFile(Package.Dte, classModel.Files.First().Path);

            Package.ClassModel = classModel;
        }

        internal static void OpenFile(EnvDTE.DTE dte, string file)
        {
            try
            {
                if (File.Exists(file))
                {
                    dte.ItemOperations.OpenFile(file);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
    }
}