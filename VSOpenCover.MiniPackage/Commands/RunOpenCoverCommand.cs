﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Threading;
using Microsoft.VisualStudio.Shell;
using VSOpenCover.CoverageProcessing;

namespace VSOpenCover.VSPackage.Commands
{
    public class RunOpenCoverCommand : CommandBase
    {
        public RunOpenCoverCommand(VSOpenCoverPackage package, CommandID commandId) : base(package, commandId)
        {

        }

        protected override void OnExecute(OleMenuCmdEventArgs eventArgs)
        {
            var selectedProjectsPaths = GetSelectedProjects();
            
            Package.UnityContainer.StoreSettings();

            var coverageProcessor = Package.UnityContainer.Resolve<ICoverageProcessor>();
            
            Package.Pane.Activate();
            var writeAction = new Action<string>(a => Package.Pane.OutputStringThreadSafe(a + "\n"));

            ThreadPool.QueueUserWorkItem(c =>
                                         {
                                            var report = coverageProcessor.MakeCoverageReport(selectedProjectsPaths, writeAction);
                                            Package.Commands[typeof(OpenCoverageResultsCommand)].Invoke(report, default(IntPtr));
                                         }, null);
        }

        private IEnumerable<string> GetSelectedProjects()
        {
            var selectedProjectsPaths = new List<string>();
            dynamic projects = Package.Dte.ActiveSolutionProjects;
            foreach (dynamic project in projects)
            {
                selectedProjectsPaths.Add(project.FileName);
            }
            return selectedProjectsPaths;
        }
    }
}