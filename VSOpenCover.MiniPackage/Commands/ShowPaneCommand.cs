﻿using System.ComponentModel.Design;
using Microsoft.VisualStudio.Shell;

namespace VSOpenCover.VSPackage.Commands
{
    public class ShowPaneCommand : CommandBase
    {
        public ShowPaneCommand(VSOpenCoverPackage package, CommandID commandId) : base(package, commandId)
        {
        }

        protected override void OnExecute(OleMenuCmdEventArgs eventArgs)
        {
            Package.Pane.Activate();
        }
    }
}