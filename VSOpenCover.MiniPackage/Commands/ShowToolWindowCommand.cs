﻿using System;
using System.ComponentModel.Design;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using VsOpenCover.ReportGeneratorResultParser.Model.Summary;
using VSOpenCover.VSPackage.ViewModel;
using VSOpenCover.VSPackage.Views;

namespace VSOpenCover.VSPackage.Commands
{
    public class ShowToolWindowCommand : CommandBase
    {
        public ShowToolWindowCommand(VSOpenCoverPackage package, CommandID commandId) : base(package, commandId)
        {
        }

        protected override void OnExecute(OleMenuCmdEventArgs eventArgs)
        {
            ToolWindowPane window = Package.FindToolWindow(typeof(CodeCoverageResultsToolWindow), 0, false);
            if ((null == window) || (null == window.Frame))
            {
                throw new NotSupportedException(Resources.CanNotCreateWindow);
            }

            var windowFrame = (IVsWindowFrame)window.Frame;
            ErrorHandler.ThrowOnFailure(windowFrame.Show());

            var value = eventArgs.InValue as SummaryCoverageReport;
            if (value != null)
            {
                var report = value;
                var view = ((CoverageResultView)window.Content);

                var viewModel = new CodeCoverageViewModel(report);
                viewModel.OpenFileCommand = Package.Commands[typeof (OpenFileCoverageCommand)];

                view.Initialize(viewModel);
            }
        }
    }
}