﻿using EnvDTE;
using VSOpenCover.SharedTypes.Settings;

namespace VSOpenCover.VSPackage.DependencyInjection.Components
{
    public interface ISettingsLoader
    {
        IVsOpenCoverSettings LoadSettings();
    }
}