﻿using System;
using EnvDTE;
using VSOpenCover.CoverageProcessing.Annotations;
using VSOpenCover.SharedTypes;
using VSOpenCover.SharedTypes.Settings;

namespace VSOpenCover.VSPackage.DependencyInjection.Components
{
    [UsedImplicitly]
    public sealed class SettingsLoader : ISettingsLoader
    {
        private readonly DTE _settingsDTE;

        public SettingsLoader(DTE settingsDTE)
        {
            _settingsDTE = settingsDTE;
        }

        public IVsOpenCoverSettings LoadSettings()
        {
            Properties properties = _settingsDTE.Properties["VSOpenCover", "OptionPage"];
            dynamic doptions = properties;
            var outputType = (ReportGeneratorOutputType)doptions["OutputType"].Value;
            
            var settings = new VsOpenCoverSettings
            {
                //OpenCover
                TestRunnerExePath = doptions["TestRunnerPath"].Value,
                OpenCoverExePath = doptions["OpenCoverPath"].Value,
                OutputDirectory = doptions["OutputDirectory"].Value,
                OpenCoverFilter = doptions["OpenCoverFilter"].Value,
                Framework = doptions["Framework"].Value,
                //ReportGenerator
                ReportGeneratorExePath = doptions["ReportGeneratorPath"].Value,
                ReportGeneratorOutputDirectory =
                        String.Format(@"{0}\{1}Report", doptions["OutputDirectory"].Value, outputType.ToString()),
                OutputType = outputType,
                InputFilesMask = String.Format(@"{0}\*.xml", doptions["OutputDirectory"].Value)
            };
            return settings;
        } 
    }
}