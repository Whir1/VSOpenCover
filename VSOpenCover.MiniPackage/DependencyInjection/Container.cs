﻿using EnvDTE;
using Microsoft.Practices.Unity;
using OpenCoverRunner;
using OpenCoverRunner.ProcessInfoBuilders;
using OpenCoverRunner.ProcessInfoBuilders.ArgumentFormatters;
using VSOpenCover.CoverageProcessing;
using VSOpenCover.CoverageProcessing.Processors;
using VsOpenCover.ReportGeneratorResultParser;
using VSOpenCover.SharedTypes.Settings;
using VSOpenCover.VSPackage.DependencyInjection.Components;

namespace VSOpenCover.VSPackage.DependencyInjection
{
    public class Container
    {
        private readonly IUnityContainer _unityContainer;
        public Container()
        {
            _unityContainer = new UnityContainer();
        }

        public void Initialize(DTE settingsDTE)
        {
            _unityContainer.RegisterInstance(settingsDTE);
            _unityContainer.RegisterType<ISettingsLoader, SettingsLoader>();
            
            StoreSettings();
            
            RegisterCoverageComponents();
        }

        private void RegisterCoverageComponents()
        {
            
            _unityContainer.RegisterType<IProcessInfoBuilder, OpenCoverBuilder>("OpenCoverBuilder");
            _unityContainer.RegisterType<IProcessInfoBuilder, ReportGeneratorBuilder>("ReportGeneratorBuilder");
            _unityContainer.RegisterType<IProcessInfoFabric, ProcessInfoFabric>();

            _unityContainer.RegisterType<IReportProcessor, ReportProcessor>();
            _unityContainer.RegisterType<IFileProcessor, FileProcessor>();

            _unityContainer.RegisterType<IRunner, Runner>();

            _unityContainer.RegisterType<IArgumentFormatter, OpenCoverArgumentFormatter>("OpenCoverArgumentFormatter");
            _unityContainer.RegisterType<IArgumentFormatter, ReportGeneratorArgumentFormatter>("ReportGeneratorArgumentFormatter");

            _unityContainer.RegisterInstance<IReportParser>(new ReportParser());
            _unityContainer.RegisterType<ICoverageProcessor, CoverageProcessor>();
        }


        public void StoreSettings()
        {
            var settingsLoader = _unityContainer.Resolve<ISettingsLoader>();
            var settings = settingsLoader.LoadSettings();

            _unityContainer.RegisterInstance(settings);
            _unityContainer.RegisterInstance<IReportGeneratorSettings>(settings);
            _unityContainer.RegisterInstance<IOpenCoverSettings>(settings);
        }

        public IVsOpenCoverSettings ResolveSettings()
        {
            return _unityContainer.Resolve<IVsOpenCoverSettings>();
        }

        public IReportParser ResolverReportParser()
        {
            return _unityContainer.Resolve<IReportParser>();
        }

        public T Resolve<T>() where T : class
        {
            return _unityContainer.Resolve<T>();
        }
    }
}