﻿// Guids.cs
// MUST match guids.h

using System;

namespace VSOpenCover.VSPackage
{
    static class GuidList
    {
        // ReSharper disable InconsistentNaming
        public const string guidVSOpenCoverPackagePkgString = "ccfe54c9-f615-4f9d-b4ba-ccdcb37df107";
        public const string guidVSOpenCoverPackageCmdSetString = "18871bcc-2f32-4827-a158-4e5870c0c880";
        public const string guidVSOpenCoverCoverageResultToolWindow = "276D0856-1101-4FA4-849D-6094915D6CF4";

        public static readonly Guid guidVSOpenCoverPackageCmdSet = new Guid(guidVSOpenCoverPackageCmdSetString);
        public static readonly Guid guidVSOpenCoverPackageContextMenuCmdSet = new Guid(guidVSOpenCoverPackageCmdSetString);
        // ReSharper restore InconsistentNaming
    };
}