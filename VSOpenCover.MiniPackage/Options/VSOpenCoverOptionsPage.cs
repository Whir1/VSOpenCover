﻿using System;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio.Shell;
using VSOpenCover.SharedTypes;

namespace VSOpenCover.VSPackage.Options
{
    [CLSCompliant(false), ComVisible(true)]
    [ClassInterface(ClassInterfaceType.AutoDual)] 
    public class VsOpenCoverOptionsPage : DialogPage
    {
        private string _testRunnerPath = @"C:\Tools\NUnit-2.6.3\bin\nunit-console.exe";
        private string _openCoverPath = @"C:\Tools\opencover.4.5.1604\OpenCover.Console.exe";
        private string _outputDirectory = @"C:\Results";
        private string _openCoverFilter = "+[CoBrand*]* -[*SharedTypes]* -[*Tests]*";
        private string _framework = "net-4.0";
        private string _reportGeneratorExePath = @"C:\Tools\ReportGenerator_1.9.0.0\bin\ReportGenerator.exe";
        private ReportGeneratorOutputType _outputType = ReportGeneratorOutputType.Html;

        public string TestRunnerPath
        {
            get { return _testRunnerPath; }
            set { _testRunnerPath = value; }
        }

        public string OpenCoverPath
        {
            get { return _openCoverPath; }
            set { _openCoverPath = value; }
        }

        public string OutputDirectory
        {
            get { return _outputDirectory; }
            set { _outputDirectory = value; }
        }

        public string OpenCoverFilter
        {
            get { return _openCoverFilter; }
            set { _openCoverFilter = value; }
        }

        public string Framework
        {
            get { return _framework; }
            set { _framework = value; }
        }

        public string ReportGeneratorPath
        {
            get { return _reportGeneratorExePath; }
            set { _reportGeneratorExePath = value; }
        }

        public ReportGeneratorOutputType OutputType
        {
            get { return _outputType; }
            set { _outputType = value; }
        }
    }
}