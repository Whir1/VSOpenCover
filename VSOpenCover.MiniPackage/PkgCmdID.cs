﻿// PkgCmdID.cs
// MUST match PkgCmdID.h

namespace VSOpenCover.VSPackage
{
    // ReSharper disable once InconsistentNaming
    static class PkgCmdIDList
    {
        public const uint RunOpenCover =        0x100;
        public const uint OpenCoverageResults = 0x110;
        public const uint OpenCoverageResultsWindow = 0x120;
        public const uint OpenFile = 0x130;
        public const uint ShowWindowPane = 0x140;
    };
}