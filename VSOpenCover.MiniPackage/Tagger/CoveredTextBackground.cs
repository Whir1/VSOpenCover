﻿using System.ComponentModel.Composition;
using System.Windows.Media;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Utilities;

namespace VSOpenCover.VSPackage.Tagger
{
    [Export(typeof(EditorFormatDefinition))]
    [ClassificationType(ClassificationTypeNames = "text-background-covered")]
    [Name("text-background-covered")]
    [UserVisible(true)]
    [Order(After = Priority.High)]
    public sealed class CoveredTextBackground : ClassificationFormatDefinition
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CoveredTextBackground"/> class.
        /// </summary>
        public CoveredTextBackground()
        {
            DisplayName = "Covered Text Background";
            BackgroundColor = Colors.LightGreen;
        }
    }
}