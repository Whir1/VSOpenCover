﻿using System.ComponentModel.Composition;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Utilities;
using VSOpenCover.CoverageProcessing.Annotations;

namespace VSOpenCover.VSPackage.Tagger
{
    public static class CoveredTextBackgroundTypeExports
    {
        [Export(typeof(ClassificationTypeDefinition))]
        [Name("text-background-covered"), UsedImplicitly]
        public static ClassificationTypeDefinition OrdinaryClassificationType;
    }
}