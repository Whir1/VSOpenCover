﻿using System.ComponentModel.Composition;
using System.Windows.Media;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Utilities;

namespace VSOpenCover.VSPackage.Tagger
{
    [Export(typeof(EditorFormatDefinition))]
    [ClassificationType(ClassificationTypeNames = "text-background-notcoverable")]
    [Name("text-background-notcoverable")]
    [UserVisible(true)]
    [Order(After = Priority.High)]
    public sealed class NotCoverableTextBackground : ClassificationFormatDefinition
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CoveredTextBackground"/> class.
        /// </summary>
        public NotCoverableTextBackground()
        {
            DisplayName = "Covered Text Background";
            BackgroundColor = Colors.Gray;
        }
    }
}
