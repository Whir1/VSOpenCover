﻿using System.ComponentModel.Composition;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Utilities;
using OpenCoverRunner.Annotations;

namespace VSOpenCover.VSPackage.Tagger
{
    public static class NotCoverableTextBackgroundTypeExports
    {
        [Export(typeof(ClassificationTypeDefinition))]
        [Name("text-background-notcoverable"), UsedImplicitly]
        public static ClassificationTypeDefinition OrdinaryClassificationType;
    }
}
