﻿using System.ComponentModel.Composition;
using System.Windows.Media;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Utilities;

namespace VSOpenCover.VSPackage.Tagger
{
    [Export(typeof(EditorFormatDefinition))]
    [ClassificationType(ClassificationTypeNames = "text-background-notcovered")]
    [Name("text-background-notcovered")]
    [UserVisible(true)]
    [Order(After = Priority.High)]
    public sealed class NotCoveredTextBackground : ClassificationFormatDefinition
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CoveredTextBackground"/> class.
        /// </summary>
        public NotCoveredTextBackground()
        {
            DisplayName = "Not Covered Text Background";
            BackgroundColor = Colors.Red;
        }
    }
}