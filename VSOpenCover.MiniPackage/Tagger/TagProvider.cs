﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Text.Operations;
using Microsoft.VisualStudio.Text.Tagging;
using Microsoft.VisualStudio.Utilities;
using VSOpenCover.SharedTypes;

namespace VSOpenCover.VSPackage.Tagger
{
    [Export(typeof(IViewTaggerProvider))]
    [ContentType("CSharp")]
    [TagType(typeof(ClassificationTag))]
    internal class TagProvider : IViewTaggerProvider
    {
        [Import]
        IClassificationTypeRegistryService registry = null;

        [Import]
        ITextSearchService textSearch = null;

        public ITagger<T> CreateTagger<T>(ITextView textView, ITextBuffer buffer) where T : ITag
        {
            var tagsDictionary = new Dictionary<CoverState, ClassificationTag>
                                 {
                                         {CoverState.Covered, new ClassificationTag(registry.GetClassificationType("text-background-covered"))},
                                         {CoverState.NotCovered, new ClassificationTag(registry.GetClassificationType("text-background-notcovered"))},
                                         {CoverState.NotCoverable , new ClassificationTag(registry.GetClassificationType("text-background-notcoverable"))}
                                 };
            return (ITagger<T>)new Tagger(textView, textSearch, tagsDictionary);
        }
    }
}