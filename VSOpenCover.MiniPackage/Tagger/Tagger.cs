﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Text.Operations;
using Microsoft.VisualStudio.Text.Tagging;
using VSOpenCover.SharedTypes;

namespace VSOpenCover.VSPackage.Tagger
{
    internal class Tagger : ITagger<ClassificationTag>
    {
        private readonly ITextView _view;
        private readonly ITextSearchService _searchService;
        private readonly Dictionary<CoverState, ClassificationTag> _tagsDict;
        private List<SnapshotSpan> _currentSpans;
        private readonly VSOpenCoverPackage _package;

        public Tagger(ITextView view, ITextSearchService searchService, Dictionary<CoverState, ClassificationTag> tagsDict)
        {
            _view = view;
            _searchService = searchService;
            _tagsDict = tagsDict;

            _package = VSOpenCoverPackage.PackageInstance;

            //_view.GotAggregateFocus += SetupSelectionChangedListener;
        }

        public IEnumerable<ITagSpan<ClassificationTag>> GetTags(NormalizedSnapshotSpanCollection spans)
        {
            var result = new List<ITagSpan<ClassificationTag>>();
            if (_package.ClassModel != null)
            {
                var openedFilePath = GetFilePath(_view);
                if (openedFilePath.Equals(_package.ClassModel.Files[0].Path, StringComparison.CurrentCultureIgnoreCase))
                {

                    foreach (SnapshotSpan span in spans)
                    {
                        var spanText = span.GetText().Trim(new []{'\r', '\n', '\t', ' '});
                        var file = _package.ClassModel.Files[0];

                        var lineAnalyses = (from a in file.LineAnalyses
                                         where spanText.Equals(a.Content.Trim())
                                         select a).FirstOrDefault();
                        if (lineAnalyses != null)
                        {
                            result.Add(new TagSpan<ClassificationTag>(span, _tagsDict[lineAnalyses.CoverState]));    
                        }
                    }
                }
            }
            return result;
        }

        public event EventHandler<SnapshotSpanEventArgs> TagsChanged;

        public static string GetFilePath(ITextView textView)
        {
            ITextDocument document;
            if ((textView == null) ||
                    (!textView.TextDataModel.DocumentBuffer.Properties.TryGetProperty(typeof(ITextDocument), out document)))
                return String.Empty;

            if ((document == null) || (document.TextBuffer == null))
                return String.Empty;

            return document.FilePath;
        }
    }
}