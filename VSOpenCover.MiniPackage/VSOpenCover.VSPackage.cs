﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Runtime.InteropServices;
using EnvDTE;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using VsOpenCover.ReportGeneratorResultParser.Model.Class;
using VSOpenCover.VSPackage.Commands;
using VSOpenCover.VSPackage.DependencyInjection;
using VSOpenCover.VSPackage.Options;
using VSOpenCover.VSPackage.Views;

namespace VSOpenCover.VSPackage
{
    [PackageRegistration(UseManagedResourcesOnly = true)]
    [InstalledProductRegistration("#110", "#112", "1.0", IconResourceID = 400)]
    [ProvideMenuResource("Menus.ctmenu", 1)]
    [ProvideOptionPage(typeof(VsOpenCoverOptionsPage), "VSOpenCover", "OptionPage", 1000, 1001, true)]
    [ProvideToolWindow(typeof(CodeCoverageResultsToolWindow), MultiInstances = false, Style = VsDockStyle.Tabbed, Orientation = ToolWindowOrientation.Bottom, Window = EnvDTE.Constants.vsWindowKindOutput)]
    [Guid(GuidList.guidVSOpenCoverPackagePkgString)]
    // ReSharper disable once InconsistentNaming
    public sealed class VSOpenCoverPackage : Package
    {
        internal Container UnityContainer { get; private set; }

        internal static VSOpenCoverPackage PackageInstance { get; private set; }

        internal DTE Dte { get; private set; }

        internal DTE SDte { get; private set; }

        public ClassCoverageReport ClassModel { get; set; }

        internal IVsOutputWindowPane Pane { get; private set; }

        internal ToolWindowPane ToolWindow { get; private set; }

        internal Dictionary<Type, CommandBase> Commands { get; private set; }

        protected override void Initialize()
        {
            base.Initialize();
            
            Dte = (DTE)GetGlobalService(typeof(DTE));
            SDte = (DTE)GetService(typeof(SDTE));
            UnityContainer = InitContainer(SDte);

            Commands = new Dictionary<Type, CommandBase>();
            var mcs = GetService(typeof(IMenuCommandService)) as OleMenuCommandService;
            if ( null != mcs )
            {
                var runOpenCoverCommandId = new CommandID(GuidList.guidVSOpenCoverPackageCmdSet, (int)PkgCmdIDList.RunOpenCover);
                var runOpenCoverMenuItem = new RunOpenCoverCommand(this, runOpenCoverCommandId);
                mcs.AddCommand(runOpenCoverMenuItem);
                Commands.Add(typeof(RunOpenCoverCommand), runOpenCoverMenuItem);

                var openResultsCommandId = new CommandID(GuidList.guidVSOpenCoverPackageCmdSet, (int)PkgCmdIDList.OpenCoverageResults);
                var runOpenCoverCommand = new OpenCoverageResultsCommand(this, openResultsCommandId);
                Commands.Add(typeof(OpenCoverageResultsCommand), runOpenCoverCommand);

                var openFileCommandId = new CommandID(GuidList.guidVSOpenCoverPackageCmdSet, (int)PkgCmdIDList.OpenFile);
                var openFileCommand = new OpenFileCoverageCommand(this, openFileCommandId);
                Commands.Add(typeof(OpenFileCoverageCommand), openFileCommand);

                var showToolWindowCommandId = new CommandID(GuidList.guidVSOpenCoverPackageCmdSet, (int)PkgCmdIDList.OpenCoverageResultsWindow);
                var showToolWindowCommand = new ShowToolWindowCommand(this, showToolWindowCommandId);
                Commands.Add(typeof(ShowToolWindowCommand), showToolWindowCommand);
                mcs.AddCommand(showToolWindowCommand);

                Pane = CreateWindowPane();
                ToolWindow = FindToolWindow(typeof(CodeCoverageResultsToolWindow), 0, true);
            }
            PackageInstance = this;
        }

        private static Container InitContainer(DTE settingsDte)
        {
            var unityContainer = new Container();
            unityContainer.Initialize(settingsDte);
            return unityContainer;
        }

        private static IVsOutputWindowPane CreateWindowPane()
        {
            var outputWindow = (IVsOutputWindow) GetGlobalService(typeof (SVsOutputWindow));
            Guid mainWindowPaneId = VSConstants.OutputWindowPaneGuid.GeneralPane_guid;
            outputWindow.CreatePane(mainWindowPaneId, "VSOpenCover", 1, 1);
            IVsOutputWindowPane pane;
            outputWindow.GetPane(mainWindowPaneId, out pane);
            return pane;
        }
    }
}
