﻿using System.Windows.Input;
using VsOpenCover.ReportGeneratorResultParser.Model.Summary;

namespace VSOpenCover.VSPackage.ViewModel
{
    public class CodeCoverageViewModel : ViewModelBase
    {
        private SummaryCoverageReport _coverageReport;

        public CodeCoverageViewModel(SummaryCoverageReport coverageReport)
        {
            CoverageReport = coverageReport;
        }

        public ICommand OpenFileCommand { get; set; }

        public SummaryCoverageReport CoverageReport
        {
            get
            {
                return _coverageReport;
            }
            set
            {
                _coverageReport = value;
                OnPropertyChanged();
            }
        }
    }
}