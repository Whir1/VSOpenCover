﻿using System.Threading;
using System.Windows.Controls;
using System.Windows.Threading;
using VSOpenCover.VSPackage.ViewModel;

namespace VSOpenCover.VSPackage.Views
{
    /// <summary>
    /// Interaction logic for CoverageResult.xaml
    /// </summary>
    public partial class CoverageResultView : UserControl
    {
        public CoverageResultView()
        {
            InitializeComponent();
        }

        public void Initialize(CodeCoverageViewModel viewModel)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                                       new ThreadStart(() => DataContext = viewModel));
            }
            else
            {
                DataContext = viewModel;
            }
        }
    }
}
