﻿using System;

namespace VSOpenCover.SharedTypes
{
    [Serializable]
    public enum CoverState
    {
        Covered,
        NotCoverable,
        NotCovered
    }
}