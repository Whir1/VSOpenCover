﻿namespace VSOpenCover.SharedTypes
{
    public enum ProcessType
    {
        OpenCover,
        ReportGenerator
    }
}