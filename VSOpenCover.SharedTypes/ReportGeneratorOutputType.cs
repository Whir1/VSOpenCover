﻿using System;
using System.Runtime.InteropServices;

namespace VSOpenCover.SharedTypes
{
    [Serializable]
    [CLSCompliant(false), ComVisible(true)]
    public enum ReportGeneratorOutputType
    {
        Xml, 
        Html
    }
}