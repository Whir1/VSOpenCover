﻿namespace VSOpenCover.SharedTypes.Settings
{
    public interface IOpenCoverSettings
    {
        string TestRunnerExePath { get; set; }
        string OpenCoverExePath { get; set; }
        string Framework { get; set; }
        string OpenCoverFilter { get; set; }
        string OutputDirectory { get; set; }
    }
}