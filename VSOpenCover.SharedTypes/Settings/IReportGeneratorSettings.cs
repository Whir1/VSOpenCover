﻿namespace VSOpenCover.SharedTypes.Settings
{
    public interface IReportGeneratorSettings
    {
        string ReportGeneratorExePath { get; set; }
        string ReportGeneratorOutputDirectory { get; set; }
        ReportGeneratorOutputType OutputType { get; set; }
        string InputFilesMask { get; set; }
    }
}