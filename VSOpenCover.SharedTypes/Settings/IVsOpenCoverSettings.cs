﻿namespace VSOpenCover.SharedTypes.Settings
{
    public interface IVsOpenCoverSettings : IOpenCoverSettings, 
                                            IReportGeneratorSettings
    {
    }
}