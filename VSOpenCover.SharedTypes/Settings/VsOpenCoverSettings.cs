﻿namespace VSOpenCover.SharedTypes.Settings
{
    public class VsOpenCoverSettings : IVsOpenCoverSettings
    {
        #region OpenCoverSettings
        public string TestRunnerExePath { get; set; }
        public string OpenCoverExePath { get; set; }
        public string Framework { get; set; }
        public string OpenCoverFilter { get; set; }
        public string OutputDirectory { get; set; }

        #endregion

        #region ReportGeneratorSettings
        public string ReportGeneratorExePath { get; set; }
        public string ReportGeneratorOutputDirectory { get; set; }
        public ReportGeneratorOutputType OutputType { get; set; }
        public string InputFilesMask { get; set; }

        #endregion
    }
}