﻿using System.Xml;
using System.Xml.Serialization;

namespace VSOpenCover.SharedTypes
{
    public static class XmlLoader
    {
        public static T Load<T>(XmlNode configNode)
        {
            using (var reader = new XmlNodeReader(configNode))
            {
                var serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(reader);
            }
        }
    }
}