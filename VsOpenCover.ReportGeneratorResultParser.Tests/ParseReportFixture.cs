﻿using NUnit.Framework;
using VsOpenCover.ReportGeneratorResultParser.Model.Summary;

namespace VsOpenCover.ReportGeneratorResultParser.Tests
{
    [TestFixture]
    public class ParseReportFixture
    {
        private ReportParser _target;

        [SetUp]
        public void SetUp()
        {
            _target = new ReportParser();
        }

        [Test]
        [Category("Integrated")]
        public void WhenParseReportFile_ThenModelEqualsExpectedModel()
        {
            SummaryCoverageReport actual = _target.ParseSummaryReportFile(@".\TestFiles\XmlReport\Summary.xml");
        }

        [Test]
        [Category("Integrated")]
        public void WhenParseClassReportFile_ThenModelEqualsExpectedModel()
        {
            var actual = _target.ParseClassCoverageReport(@".\TestFiles\XmlReport\CoBrand.AlfaBank.Processing.Scores_CoBrand.AlfaBank.Processing.Scores.ProcessingComponents.ScoreLogProcessor.xml");
        }

    }
}