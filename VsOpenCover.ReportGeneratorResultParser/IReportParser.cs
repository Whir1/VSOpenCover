﻿using VsOpenCover.ReportGeneratorResultParser.Model.Class;
using VsOpenCover.ReportGeneratorResultParser.Model.Summary;

namespace VsOpenCover.ReportGeneratorResultParser
{
    public interface IReportParser
    {
        SummaryCoverageReport ParseSummaryReportFile(string filePath);
        ClassCoverageReport ParseClassCoverageReportFromModel(Assembly assembly, string className, string directory);
    }
}