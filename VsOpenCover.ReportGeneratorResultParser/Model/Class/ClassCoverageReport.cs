﻿using System.Xml.Serialization;

namespace VsOpenCover.ReportGeneratorResultParser.Model.Class
{
    [XmlRoot("CoverageReport")]
    public class ClassCoverageReport
    {
        [XmlAttribute("scope")]
        public string Scope { get; set; }

        [XmlElement("Summary")]
        public ClassSummary ClassSummary { get; set; }

        [XmlArray("Files")]
        [XmlArrayItem("File")]
        public File[] Files { get; set; }
    }
}