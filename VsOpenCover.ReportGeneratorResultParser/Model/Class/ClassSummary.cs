﻿using System.Xml.Serialization;

namespace VsOpenCover.ReportGeneratorResultParser.Model.Class
{
    [XmlRoot]
    public class ClassSummary
    {
        [XmlElement("Class")]
        public string ClassName { get; set; }

        [XmlElement("Assembly")]
        public string AssemblyName { get; set; }

        [XmlArray]
        [XmlArrayItem("File")]
        public string[] Files { get; set; }

        [XmlElement("Coverage")]
        public string CoveragePercent { get; set; }

        [XmlElement("Coveredlines")]
        public int CoveredlinesCount { get; set; }

        [XmlElement("Uncoveredlines")]
        public int UncoveredlinesCount { get; set; }

        [XmlElement("Coverablelines")]
        public int CoverablelinesCount { get; set; }

        [XmlElement("Totallines")]
        public int TotallinesCount { get; set; }
    }
}