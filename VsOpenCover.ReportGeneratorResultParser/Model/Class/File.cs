﻿using System.Xml.Serialization;

namespace VsOpenCover.ReportGeneratorResultParser.Model.Class
{
    [XmlRoot]
    public class File
    {
        [XmlAttribute("name")]
        public string Path { get; set; }

        [XmlElement("LineAnalysis")]
        public LineAnalysis[] LineAnalyses { get; set; }
    }
}