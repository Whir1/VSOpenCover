﻿using System.Xml.Serialization;
using VSOpenCover.SharedTypes;

namespace VsOpenCover.ReportGeneratorResultParser.Model.Class
{
    [XmlRoot]
    public class LineAnalysis
    {
        [XmlAttribute("line")]
        public int Line { get; set; }

        [XmlAttribute("visits")]
        public int Visits { get; set; }

        [XmlAttribute("coverage")]
        public CoverState CoverState { get; set; }

        [XmlAttribute("content")]
        public string Content { get; set; }
    }
}