using System.Xml.Serialization;

namespace VsOpenCover.ReportGeneratorResultParser.Model.Summary
{
    [XmlRoot("Assembly")]
    public class Assembly
    {
        //name="CoBrand.AlfaBank.Processing.Discounts" classes="14" coverage="100" coveredlines="341" coverablelines="341" totallines="739"
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("classes")]
        public int ClassesCount { get; set; }

        [XmlAttribute("coverage")]
        public string CoveragePercent { get; set; }

        [XmlAttribute("coveredlines")]
        public int CoveredLinesCount { get; set; }

        [XmlAttribute("coverablelines")]
        public int CoverablelinesCount { get; set; }

        [XmlAttribute("totallines")]
        public int TotallinesCount { get; set; }

        [XmlElement("Class")]
        public Class[] Classes { get; set; }
    }
}