using System.Xml.Serialization;

namespace VsOpenCover.ReportGeneratorResultParser.Model.Summary
{
    [XmlRoot]
    public class Class
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("coverage")]
        public string Coverage { get; set; }

        [XmlAttribute("coveredlines")]
        public int Coveredlines { get; set; }

        [XmlAttribute("coverablelines")]
        public int Coverablelines { get; set; }

        [XmlAttribute("totallines")]
        public int Totallines { get; set; }
    }
}