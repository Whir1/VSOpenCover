using System.Xml.Serialization;

namespace VsOpenCover.ReportGeneratorResultParser.Model.Summary
{
    public class Summary
    {
        // <Generatedon>18.04.2014 - 14:29:23</Generatedon>
        [XmlElement]
        public string Generatedon { get; set; }

        //<Parser>MultiReportParser (3x OpenCoverParser)</Parser>
        [XmlElement]
        public string Parser { get; set; }
        
        //<Assemblies>4</Assemblies>
        [XmlElement("Assemblies")]
        public int AssembliesCount { get; set; }
        
        //<Classes>33</Classes>
        [XmlElement("Classes")]
        public int ClassesCount { get; set; }
        
        //<Files>30</Files>
        [XmlElement("Files")]
        public int FilesCount { get; set; }
        
        //<Coverage>98.2%</Coverage>
        [XmlElement("Coverage")]
        public string CoveragePercent { get; set; }
        
        //<Coveredlines>631</Coveredlines>
        [XmlElement("Coveredlines")]
        public int CoveredLines { get; set; }
        
        //<Uncoveredlines>11</Uncoveredlines>
        [XmlElement("Uncoveredlines")]
        public int Uncoveredlines { get; set; }
        
        //<Coverablelines>642</Coverablelines>
        [XmlElement("Coverablelines")]
        public int Coverablelines { get; set; }
        
        //<Totallines>1456</Totallines>
        [XmlElement("Totallines")]
        public int TotalLines { get; set; }
    }
}