using System.Xml.Serialization;

namespace VsOpenCover.ReportGeneratorResultParser.Model.Summary
{
    [XmlRoot("CoverageReport")]
    public class SummaryCoverageReport
    {
        [XmlAttribute("scope")]
        public string Scope { get; set; }

        [XmlElement("Summary")]
        public Summary Summary { get; set; }

        [XmlArray("Assemblies")]
        [XmlArrayItem("Assembly")]
        public Assembly[] Assemblies { get; set; }
    }
}