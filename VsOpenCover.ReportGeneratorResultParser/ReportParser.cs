﻿using System.Xml;
using VsOpenCover.ReportGeneratorResultParser.Annotations;
using VsOpenCover.ReportGeneratorResultParser.Model.Class;
using VsOpenCover.ReportGeneratorResultParser.Model.Summary;
using VSOpenCover.SharedTypes;

namespace VsOpenCover.ReportGeneratorResultParser
{
    [UsedImplicitly]
    public class ReportParser : IReportParser
    {
        public SummaryCoverageReport ParseSummaryReportFile(string filePath)
        {
            var doc = new XmlDocument();
            doc.Load(filePath);
            
            var model = XmlLoader.Load<SummaryCoverageReport>(doc);
            return model;
        }

        public ClassCoverageReport ParseClassCoverageReport(string filePath)
        {
            var doc = new XmlDocument();
            doc.Load(filePath);

            var model = XmlLoader.Load<ClassCoverageReport>(doc);
            return model;
        }

        public ClassCoverageReport ParseClassCoverageReportFromModel(Assembly assembly, string className, string directory)
        {
            var filePath = GetClassReportFilePath(assembly, className, directory);
            return ParseClassCoverageReport(filePath);
        }

        private string GetClassReportFilePath(Assembly model, string className, string directory)
        {
            return string.Format(@"{0}\{1}_{2}.xml", directory, model.Name, className);
        }

    }
}